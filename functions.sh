#!/bin/bash

###############################################################
#
# This script contains functions and var
# for simplifying bash script
#
###############################################################
#
# Author        : J. Varnier
# Version       : 0.29
# Licence       : GPL v3
# Last Update   : 2022-07-11
#
# Changes		: Some fixes
#
###############################################################

# ------------------------------------------------------
# ------------ Globals Var -----------------------------
# ------------------------------------------------------

# /!\ This values have not to be changed for a general use /!\

# Various check var
CHECK_ROOT=FALSE						# TRUE => we are root
CHECK_MYSQL_ENV=FALSE					# TRUE => MySQL env var is OK
SCREEN_DISPLAY=${SCREEN_DISPLAY-TRUE}	# TRUE => show message on screen, FALSE => no show
SHOW_MSG_ON_SCREEN="${SCREEN_DISPLAY}"	# TRUE => show message on screen (write_scr, write_msg, write_info, write_msg_2 functions) /!\ Kept for compatibility reason /!\
# Various var
SCRIPT_NAME="No Name"					# Default script name (init script)
OPTIND=1								# For get_opts function
TEXT_STRING_FOUND=FALSE					# For search_string_on_file function, TRUE => $2 string has been found in $1 file
STOP_ON_ERROR=${STOP_ON_ERROR-FALSE}	# FALSE => if possible, continue the script, but just tell there was an error
FCT_EXEC_STATUS=0						# Global var used to indicate, for a function not making any display, if she was well executed (FCT_EXEC_STATUS=0 or if there was a problem FCT_EXEC_STATUS=1)
MAIL_PORT=25							# Port used to send mails (used in function send_telnet_mail)
# Log var
LOG_FIRST_RUN=1							# Used to know if it's the first call to write_log function during the script run (because on the first call, some msg have to be added)
LOG_MODE="666"							# Used in write_log function during the log file creation, determine the file access mode (example : 666 means rw for all)
# Screen var
SCR_VERBOSE="${SCR_VERBOSE-FALSE}"		# Used to show more details in some functions / scripts / softwares (need for the functions to implement this option, look at send_telnet_mail for example) - FALSE (default) : no verbose / other value : verbose
# Exit status var
EXIT_OK=0
EXIT_NO_ROOT=1
EXIT_ERR_FCT=2
EXIT_ERR_MYSQL=3
EXIT_ERR_ARG=4
EXIT_ERR_FILE=5
EXIT_ERR_OTHER=6
# Write_info messages type
WI_DBUG=5
WI_INFO=4
WI_WARN=3
WI_ERR=2
# Param status
PARAM_MISSING="Missing"					# used in functions to check if a parameter is missing
PARAM_NONE="None"						# used in functions to check if an optional paramater is given or not
PARAM_NODATA="No_Data"					# used in functions to check if an optional paramater has data or not
PARAM_NONAME="No name given"			# used in functions to check if a parameter has no name
# Mail var
FROM="${FROM-"some@one.com"}"
TO="${TO-"other@one.com"}"
TITLE="${TITLE-"subject: Test mail from $(hostname)"}"
BODY="${BODY-"Test mail from $(hostname)"}"
RELAY="${RELAY-"smtp.relay.srv"}"
MAIL_PORT="${MAIL_PORT-25}"
MAIL_PORT_SECURE="${MAIL_PORT_SECURE-465}"
MAIL_PAUSE_BETWEEN_CMD="${MAIL_PAUSE_BETWEEN_CMD-1}"	# sleep time in second between 2 telnet or openssl commands

# ------------------------------------------------------
# ------------ End of var ------------------------------
# ------------------------------------------------------

# ------------------------------------------------------
# ------------ Begin of functions ----------------------
# ------------------------------------------------------

# ------------------------------------------------------
# ------------ Common functions ------------------------
# ------------------------------------------------------

# ------------ Exit functions ------------------------

# exit_script	:	Function used to exit script on error
# Args			:	$1 => exit message
#					$2 => exit code (according to the mains vars EXIT_xx)
function exit_script () {
	local msg=${1-"No detail on the error"}
	local exit_code=${2-1}
	local screen_display=${SCREEN_DISPLAY-FALSE}
	local log_file=${LOG_FILE-"/tmp/no_file.log"}
	if [ ${screen_display} = TRUE ]; then
		echo ""
		echo "/!\\ Error (code ${exit_code}) : ${msg} /!\\"
	fi
	# check log file before trying to write in it
	if [ -e "${log_file}" ]; then
		# Check if the log file is writable
		if [ ! -w "${log_file}" ]; then
			if [ ${screen_display} = TRUE ]; then
				echo "/!\\ Can't write in the file '${log_file}' /!\\"
			fi
		else
			write_log "/!\\ Error (code ${exit_code}) : ${msg} /!\\"
		fi
	else
		if [ ${screen_display} = TRUE ]; then
			echo "/!\\ Can't access to the file '${log_file}' /!\\"
		fi
	fi
	if [ ${screen_display} = TRUE ]; then
		echo ""
		echo "/!\\ Exiting script with error /!\\"
		echo ""
	fi
	exit ${exit_code}
}

# ------------ Some functions -------------------------

# get_script_name	:	Define the short script name and get it back via SCRIPT_NAME
# Args				:	$1 => ${0} - script name
function get_script_name () {
	local script_name=${1-"No name given"}
	FCT_EXEC_STATUS=1
	SCRIPT_NAME=`basename ${script_name}`
	FCT_EXEC_STATUS=0
}

# check_root		:	Function used to check if current script runner is root
# Args				:	none
function check_root () {
	local user_id=$(id -u)
	FCT_EXEC_STATUS=1
	if [ ${user_id} -ne 0 ]; then
		CHECK_ROOT=FALSE
		exit_script "You need to be 'root' to run this script" ${EXIT_NO_ROOT}
	else
		CHECK_ROOT=TRUE
		write_info "[OK] Root check" ${WI_INFO}
	fi
	FCT_EXEC_STATUS=0
}

# get_opts		:	Function used to collect and analyze script's args
# Args			:	$1 => args list ("${@}" has to be passed to the function)
function get_opts () {
	local verbose=FALSE
	local arg_list=${1}	# Call the function with 'get_opts ${@}'
 	local nb_arg=0		# !!!!!! Not used for now !!!!!!
	OPTIND=1			# POSIX variable : Reset in case getopts has been used previously in the shell
	FCT_EXEC_STATUS=1
	while getopts ":h?Vvf:" opt "${arg_list}"; do	# ':' used to mask illegal option, if not, bash sho an error message (illegal option)
		case "$opt" in
			h|\?)	usage
				;;
			V)	verbose=TRUE
				;;
			v)	welcome_msg
				exit ${EXIT_OK}
				;;
			:)	exit_script "Option -$OPTARG requires an argument." ${EXIT_ERR_ARG}
				;;
		esac
	done
	shift $((OPTIND-1))
	[ "${arg_list}" = "--" ] && shift
	FCT_EXEC_STATUS=0
}

# random_number	:	return a random number between 0 and $1 starting at $2
# Args			:	$1 => max increment
# 					$2 => start value
function random_number () {
	local increment_v=${1}
	local start_v=${2}
	# 'bc' has to be installed on the computer
	return $(echo $RANDOM % ${increment_v} + ${start_v} | bc)
}

# ------------ Text functions ----------------------

# search_string_on_file	:	the function aim to search if a string is present in a
#					text file, use TEXT_STRING_FOUND to return the result (FALSE : not found
#					or problem, TRUE : found)
# Args					:	$1 => file where to search the $2 string
#							$2 => string to search in the $1 file
function search_string_on_file () {
	local file=${1}
	local str_search=${2}
	local exec_status=""
	FCT_EXEC_STATUS=1
	if [ "${file}" = "" -o "${str_search}" = "" ]; then
		TEXT_STRING_FOUND=FALSE
		if [ "${}" = TRUE ]; then
			write_info "The file ('${file}') or the string to search ('${str_search}') is/are missing" ${WI_ERR}
		else
			exit_script "The file ('${file}') or the string to search ('${str_search}') is/are missing" ${EXIT_ERR_FCT}
		fi
	else
		# check file before trying to write in it
		if [ -e "${file}" ]; then
			# Check if the file is readable
			if [ ! -r "${log_file}" ]; then
				cat "${file}" | grep "${str_search}" >/dev/null 2>&1
				exec_status=${?}
				if [ "${exec_status}" = "0" ]; then
					TEXT_STRING_FOUND=TRUE
				else
					TEXT_STRING_FOUND=FALSE
				fi
			else
				TEXT_STRING_FOUND=FALSE
				if [ "${STOP_ON_ERROR}" = FALSE ]; then
					write_info "The file '${file}' is not readable" ${WI_ERR}
				else
					exit_script "The file '${file}' is not readable" ${EXIT_ERR_FCT}
				fi
			fi
		else
			TEXT_STRING_FOUND=FALSE
			if [ "${STOP_ON_ERROR}" = FALSE ]; then
				write_info "The file '${file}' is not accessible" ${WI_ERR}
			else
				exit_script "The file '${file}' is not accessible" ${EXIT_ERR_FCT}
			fi
		fi
	fi
	FCT_EXEC_STATUS=0
}

# clean_file_del	:	Function used to remove from a text file the lines containing a list of words
# Args				:	$1 => file where to find text to clean
#						$2 => resulting file, where the cleaned text will be put (if the file exist, it will be overwritten)
#						$3 => file containing the list of words to clean
# Notes				:	$1 is not modified
#						If a word is present on a line, all the line is deleted
function clean_file_del () {
	local file_origin="${1}"
	local file_modified="${2}"
	local file_words_list="${3}"
	local file_modified_temp="${file_modified}.tmp"
	FCT_EXEC_STATUS=1
	write_info "File to analyse : ${file_origin}" ${WI_DBUG}
	write_info "Analyzed file   : ${file_modified}" ${WI_DBUG}
	write_info "Words list file : ${file_words_list}" ${WI_DBUG}
	write_info "Backuping ${file_origin} into ${file_modified}" ${WI_DBUG}
	cp "${file_origin}" "${file_modified}"
	# Clean the text
	while read line
	do
		if [ ${line:0:1} != '#' ]; then
			#echo "sed "/${line}/d" "${file_modified}" > "${file_modified_temp}"
			write_info "Word to delete      : ${line}" ${WI_DBUG}
			sed "/${line}/d" "${file_modified}" > "${file_modified_temp}"
			mv "${file_modified_temp}" "${file_modified}"
		else
			write_info "Line begining by '#' => ignored ('${line}')" ${WI_DBUG}
		fi
	done < ${file_words_list}
	# del blank lines
	sed -i '/^$/d' "${file_modified}"
	FCT_EXEC_STATUS=0
}

# clean_file_replace	:	Function used to replace word from a file by other words
# Args					:	$1 => file where to find text to replace
#							$2 => resulting file, where the replaced word will be put (if the file exist, it while be overwriten)
#							$3 => file containing the list of words to replace and the replacement words
# Note					:	$1 is not modified
function clean_file_replace () {
	local file_origin="${1}"
	local file_modified="${2}"
	local file_words_list="${3}"
	local file_modified_temp="${file_modified}.tmp"
	local word_1=""
	local word_2=""
	local search_second_word="0"
	FCT_EXEC_STATUS=1
	write_info "File to analyse : ${file_origin}" ${WI_DBUG}
	write_info "Resulting file  : ${file_modified}" ${WI_DBUG}
	write_info "Words list file : ${file_words_list}" ${WI_DBUG}
	# Make visible special char like ^[[8m or ^[[0m used by jenkins
	write_info "Make visible special char like ^[[8m or ^[[0m used by Jenkins" ${WI_DBUG}
	$(cat -v "${file_origin}" > "${file_modified}")
	# Clean the text
	while read line
	do
		# Get the first word (word to be replaced)
		if [ "${line:0:1}" != "#" -a "${search_second_word}" = "0" ]; then
			word_1=${line}
			search_second_word="1"
			write_info "Word to be replaced : ${word_1}" ${WI_DBUG}
		# Get the second word (replacement word)
		elif [ "${line:0:1}" != "#" -a "${search_second_word}" = "1" ]; then
			word_2=${line}
			search_second_word="0"
			write_info "Repleacement word   : ${word_2}" ${WI_DBUG}
			sed -e "s/${word_1}/${word_2}/g" "${file_modified}" > "${file_modified_temp}"
			mv "${file_modified_temp}" "${file_modified}"
		fi
		write_info "Next word..." ${WI_DBUG}
	done < ${file_words_list}
	FCT_EXEC_STATUS=0
}

# ------------ Mail functions ----------------------

# send_telnet_mail	:	Function used to send mail via telnet app, use MAIL_PORT
# Args				:	$1 => mail relay to be used with telnet
#						$2 => mail sender
#						$3 => mail receiver
#						$4 => mail subject
#						$5 => mail data
# Note				:	this function doesn't manage telnet or relay error message
function send_telnet_mail () {
	local relay="${1}"
	local port="${MAIL_PORT-25}"
	local from="${2}"
	local to="${3}"
	local subject="${4}"
	local data="${5}"
	local verbose=${SCREEN_DISPLAY-FALSE}
	local pause=${MAIL_PAUSE_BETWEEN_CMD-1}		# sleep time in second between 2 sent, to prevent to be considered as a spam machine
	FCT_EXEC_STATUS=1
	write_info "Send an email via telnet to '${to}'" ${WI_INFO}
	# Display msg on screen ?
	if [ "${verbose}" = FALSE ]; then
	{
(
	echo HELO "${relay}"
	sleep ${pause}
	echo "MAIL FROM:<${from}>"
	sleep ${pause}
	echo "RCPT TO:<${to}>"
	sleep ${pause}
	echo DATA
	echo "MIME-Version: 1.0"
	echo "Content-Type: text/plain; charset=\"utf-8\""
	echo "${subject}"
	echo "${data}"
	echo .
	sleep ${pause}
	echo quit
) | telnet "${relay}" "${port}" > /dev/null 2>&1
	}
	else
	{
(
	echo HELO "${relay}"
	sleep ${pause}
	echo "MAIL FROM:<${from}>"
	sleep ${pause}
	echo "RCPT TO:<${to}>"
	sleep ${pause}
	echo DATA
	echo "Content-Type: text/plain; charset=\"utf-8\""
	echo "${subject}"
	echo "${data}"
	echo .
	sleep ${pause}
	echo quit
) | telnet "${relay}" "${port}"
	}
	fi
	write_info "Mail sent" ${WI_INFO}
	FCT_EXEC_STATUS=0
}

# send_openssl_mail	:	Function used to send mail via openssl app, use MAIL_PORT
# Args				:	$1 => mail relay to be used with openssl
#						$2 => mail sender
#						$3 => mail receiver
#						$4 => mail subject
#						$5 => mail data
# Note				:	this function doesn't manage openssl or relay error message
function send_openssl_mail () {
	local relay="${1}"
	local port="${MAIL_PORT_SECURE-465}"
	local from="${2}"
	local to="${3}"
	local subject="${4}"
	local data="${5}"
	local verbose=${SCR_VERBOSE-FALSE}
	local pause=${MAIL_PAUSE_BETWEEN_CMD-1}	# sleep time in second between 2 sent, to prevent to be considered as a spam machine
	# encode in Base64 the login, need to use '\' before special char
	#local mail_user=$(perl -MMIME::Base64 -e 'print encode_base64("${MAIL_USER}");')
	#  (alternative method)
	local mail_user=$(echo -n ${MAIL_USER} | openssl base64)
	# encode in Base64 the pwd, need to use '\' before special char
	#local mail_pwd=$(perl -MMIME::Base64 -e 'print encode_base64("${MAIL_PWD}");')
	# (alternative method)
	local mail_pwd=$(echo -n ${MAIL_PWD} | openssl base64)
	FCT_EXEC_STATUS=1
	write_info "Send an email via openssl to '${to}'" ${WI_INFO}
	# Display msg on screen ?
	if [ "${verbose}" = FALSE ]; then
	{
(
	echo helo "${relay}"
	sleep ${pause}
	echo auth login
	sleep ${pause}
	echo ${mail_user}
	sleep ${pause}
	echo ${mail_pwd}
	sleep ${pause}
	echo "mail from:${from}"
	sleep ${pause}
	echo "rcpt to: ${to}"
	sleep ${pause}
	echo data
	echo "MIME-Version: 1.0"
	echo "Content-Type: text/plain; charset=\"utf-8\""
	echo "${subject}"
	echo "${data}"
	echo .
	sleep ${pause}
	echo quit
) | openssl s_client -connect "${relay}:${port}" > /dev/null 2>&1
	}
	else
	{
(
	echo helo "${relay}"
	sleep ${pause}
	echo auth login
	sleep ${pause}
	echo ${mail_user}
	sleep ${pause}
	echo ${mail_pwd}
	sleep ${pause}
	echo "mail from:${from}"
	sleep ${pause}
	echo "rcpt to: ${to}"
	sleep ${pause}
	echo data
	echo "MIME-Version: 1.0"
	echo "Content-Type: text/plain; charset=\"utf-8\""
	echo "${subject}"
	echo "${data}"
	echo .
	sleep ${pause}
	echo quit
) | openssl s_client -connect "${relay}:${port}"
	}
	fi
	local status=${?}
	if [ ${status} -eq 0 ]; then
		write_info "Mail sent" ${WI_INFO}
	else
		write_info "Error sending mail via openssl (return from openssl : ${status})" ${WI_INFO}
	fi
	FCT_EXEC_STATUS=0
}

# mail_body_add	:	Add text to mail body
# Args			:	$1 => text to add
function mail_body_add () {
  BODY="${BODY}
${1}"
}

# ------------ System functions ---------------------

# check_pid	:	Function used to check if a pid associated to a script or program is already running
# Args		:	$1 => the PID file to check
# Note		:	The function check the $1 (PID file) existence, if true, give some info
function check_pid () {
	local pid_file=${1}
	local pid_number=""
	local exit_status=""
	FCT_EXEC_STATUS=1
	# Check if pid file exist
	if [ -f ${pid_file} ]; then
		# get the pid(s) number(s)
		pid_number=$(cat ${pid_file})
		$(kill -0 ${pid_number})
		exit_status=${?}
		if [ ${exit_status} -eq 0 ]; then
			write_info "The ${pid_file} exist with a pid number ${pid_number}" ${WI_INFO}
		else
			write_info "The ${pid_file} exist but either there is no pid number (pid number found '${pid_number}') or the process is down" ${WI_WARN}
		fi
	else
		write_info "The ${pid_file} is missing" ${WI_WARN}
	fi
	FCT_EXEC_STATUS=0
}

# manage_pid	:	Function used to check if a pid associated to a script or program is already running, if yes, exit, if not, create the pid
# Args			:	$1 => the PID file to check or create
# Note			:	The function check the $1 (PID file) existence, if true,
#					exit 0 the script, if false, create the PID file.
#					The function also trap the PID file deletion on EXIT
function manage_pid () {
	local pid_file=${1}
	local pid_number=""
	local exit_status=""
	FCT_EXEC_STATUS=1
	# Check if pid file exist
	if [ -f ${pid_file} ]; then
		# get the pid(s) number(s)
		pid_number=$(cat ${pid_file})
		$(kill -0 ${pid_number})
		exit_status=${?}
		if [ ${exit_status} -eq 0 ]; then
			exit_script "Script already running, end this script before (${pid_file} is existing and pid number ${pid_number} running)" ${EXIT_OK}
		fi
	fi
	# Record PID
	echo "${$}" > ${pid_file}
	write_info "Creating PID ($(cat ${pid_file}))" ${WI_INFO}
	# Auto del PID at the end of the script
	trap "rm -f ${pid_file}" EXIT
	FCT_EXEC_STATUS=0
}

# check_service	:	Function used to check if a service is running
# Args			:	$1 => name of the service
#					$2 => type of service to check ("srv" -> run as service / "grep" -> check via grep)
function check_service () {
	local serv_name="${1-"No_Data"}"
	local serv_type="${2-"No_Type"}"
	local exit_status=""
	FCT_EXEC_STATUS=1
	write_info "Checking service '${serv_name}'" ${WI_DBUG}
	if [ "${serv_type}" = "srv" ]; then
		eval "/etc/init.d/${serv_name} status > /dev/null 2>&1"
		exit_status=${?}
		if [ "${exit_status}" = "0" ]; then
			write_info "[OK] Service '${serv_name}' running" ${WI_INFO}
		fi
	elif [ "${serv_type}" == "grep" ]; then
		exit_status="$(ps aux | grep ${serv_name} | grep -v grep | wc -l)"
		if [ ${exit_status} -gt 0 ]; then
			write_info "[OK] Service '${serv_name}' running" ${WI_INFO}
		fi
	else
		if [ "${STOP_ON_ERROR}" = FALSE ]; then
			write_info "[KO] Service '${serv_name}' status cannot be determined" ${WI_ERR}
		else
			exit_script "[KO] Service '${serv_name}' status cannot be determined" ${EXIT_ERR_FCT}
		fi
	fi
	FCT_EXEC_STATUS=0
}

# check_inode_space	:	Function used to check if an inode or a space treshold has been reached
# Args				:	$1 => inode / space , define what to check
#						$2 => treshold level,  number between 01 and 99 (means between 01% and 99%)
#						$3 => name of the result file for the 'df' command
#						$4 => file system to exclude (nothing or none = none)
function check_inode_space () {
	local mode="${1-"space"}"
	local level="${2-"50"}"
	local result_file="${3-"/tmp/result.lst"}"
	local excl_sysfs="${4-"none"}"
	local exit_status=""
	local value=""
	if [ "${level:0:1}" = "0" ]; then
		local alarm_level="-e '[${level:1:1}-9]\%'" 				# In regex format => example : -e '[5-9][0-9]\%' means between 50 to 99%
	else
		local alarm_level="-e '[${level:0:1}-9][${level:1:1}-9]\%'" # In regex format => example : -e '[5-9][0-9]\%' means between 50 to 99%
	fi
	local first_title_call=TRUE
	FCT_EXEC_STATUS=1
	if [ "${mode}" = "space" ]; then
		eval "df -Ph | grep ${alarm_level} >> ${result_file}"
		exit_status=${?}
		if [ "${exit_status}" = "1" ]; then
			write_info "[OK] Treshold level (${level}%) for disk usage has not been reached" ${WI_INFO}
		else
			if [ "${excl_sysfs}" = "none" ]; then
						write_info "[KO] Treshold level (${level}%) for disk usage has been reached :" ${WI_ERR}
				write_info "/!\\ (disk) (total_space) (used_space) (free_space) (%_used) (mount_point)" ${WI_ERR}
				while read line
				do
					write_info "/!\\ ${line}" ${WI_ERR}
				done < ${result_file}
			else
				while read line
					do
						eval "echo ${line} | grep ${excl_sysfs} > /dev/null 2>&1"
						exit_status=${?}
						if [ "${exit_status}" = "1" ]; then
							if [ "${first_title_call}" = TRUE ]; then
								first_title_call=FALSE
								write_info "[KO] Treshold level (${level}%) for disk usage has been reached :" ${WI_ERR}
								write_info "/!\\ (disk) (total_space) (used_space) (free_space) (%_used) (mount_point)" ${WI_ERR}
								write_info "/!\\ ${line}" ${WI_ERR}
							else
								write_info "/!\\ ${line}" ${WI_ERR}
							fi
						fi
					done < ${result_file}
					if [ "${first_title_call}" = TRUE ]; then
						write_info "[OK] Treshold level (${level}%) for disk usage has not been reached" ${WI_INFO}
					fi
			fi
		fi
	elif [ "${mode}" == "inode" ]; then
		eval "df -iPh | grep ${alarm_level} >> ${result_file}"
		exit_status=${?}
		if [ "${exit_status}" == "1" ]; then
			write_info "[OK] Treshold level (${level}%) for inode usage hasn't been reached" ${WI_INFO}
		else
			if [ "${excl_sysfs}" == "none" ]; then
				write_info "[KO] Treshold level (${level}%) for inode usage has been reached :" ${WI_ERR}
        		write_info "/!\\ (disk) (total_space) (inode_space) (free_inode) (%_used) (mount_point)" ${WI_ERR}
				while read line
				do
				write_info "/!\\ ${line}" ${WI_ERR}
				done < ${result_file}
			else
				while read line
				do
				eval "echo ${line} | grep ${excl_sysfs} > /dev/null 2>&1"
				exit_status=${?}
				if [ "${exit_status}" == "1" ]; then
					if [ "${first_title_call}" == "TRUE" ]; then
					first_title_call="FALSE"
					write_info "[KO] Treshold level (${level}%) for inode usage has been reached :" ${WI_ERR}
					write_info "/!\\ (disk) (total_space) (inode_space) (free_inode) (%_used) (mount_point)" ${WI_ERR}
					write_info "/!\\ ${line}" ${WI_ERR}
					else
					write_info "/!\\ ${line}" ${WI_ERR}
					fi
				fi
				done < ${result_file}
				if [ "${first_title_call}" == "TRUE" ]; then
					write_info "[OK] Treshold level (${level}%) for inode usage hasn't been reached" ${WI_INFO}
				fi
			fi
		fi
	else
		if [ "${STOP_ON_ERROR}" = FALSE ]; then
			write_info "[KO] Type of measure not known (waiting for 'inode' or 'space', '${mode}' given)" ${WI_ERR}
		else
			exit_script "[KO] Type of measure not known (waiting for 'inode' or 'space', '${mode}' given)" ${EXIT_ERR_FCT}
		fi
	fi
	rm -Rf ${result_file} > /dev/null 2>&1
	FCT_EXEC_STATUS=0
}

# check_file	:	Function used to check if a file is OK (EXIST_FILE-READ_FILE-WRITE_FILE-IS_FILE-IS_FOLDER-EXIST_CREATE-DIR_EXIST_CREATE-CREATE)
# Args			:	$1 => file to check (mandatory)
#					$2 => type of check to make (EXIST_FILE-READ_FILE-WRITE_FILE-IS_FILE-IS_FOLDER-EXIST_CREATE-DIR_EXIST_CREATE-CREATE) (optional : if not defined, make an "EXIST" check)
# Note			:	Theses actions have to be completed in futures "functions.sh" releases
function check_file () {
	local file_to_chk="${1-"${PARAM_MISSING}"}"
	local action_to_do="${2-"EXIST"}"
	if [ "${file_to_chk}" = "${PARAM_MISSING}" ]; then
		exit_script "No file to check" ${EXIT_ERR_FILE}
	fi
	if [ "${action_to_do}" = "EXIST_FILE" ]; then
		if [ -e "${file_to_chk}" ]; then
			write_info "Access to file \"${file_to_chk}\" OK" ${WI_DBUG}
		else
			write_info "Can't access to file \"${file_to_chk}\"" ${WI_ERR}
			exit_script "File is missing ?" ${EXIT_ERR_FILE}
		fi
	elif [ "${action_to_do}" = "READ_FILE" ]; then
		if [ -e "${file_to_chk}" ]; then
			if [ -r "${file_to_chk}" ]; then
				write_info "Reading access to file \"${file_to_chk}\" OK" ${WI_DBUG}
			else
				write_info "Can't read the file \"${file_to_chk}\"" ${WI_ERR}
				exit_script "Access mode or user's autorization problem ?" ${EXIT_ERR_FILE}
			fi
		else
			write_info "Can't access to file \"${file_to_chk}\"" ${WI_ERR}
			exit_script "File is missing ?" ${EXIT_ERR_FILE}
		fi
	elif [ "${action_to_do}" = "WRITE_FILE" ]; then
		if [ -e "${file_to_chk}" ]; then
			if [ -w "${file_to_chk}" ]; then
				write_info "Writing access to file \"${file_to_chk}\" OK" ${WI_DBUG}
			else
				write_info "Can't write in the file \"${file_to_chk}\"" ${WI_ERR}
				exit_script "Access mode or user's autorization problem ?" ${EXIT_ERR_FILE}
			fi
		else
			write_info "Can't access to file \"${file_to_chk}\"" ${WI_ERR}
			exit_script "File is missing ?" ${EXIT_ERR_FILE}
		fi
	elif [ "${action_to_do}" = "IS_FOLDER" ]; then
		if [ -d "${file_to_chk}" ]; then
			write_info "The folder \"${file_to_chk}\" exist" ${WI_DBUG}
		else
			write_info "Can't access to the folder \"${file_to_chk}\"" ${WI_ERR}
			exit_script "Folder is missing ?" ${EXIT_ERR_FILE}
		fi
	elif [ "${action_to_do}" = "IS_FILE" ]; then
		if [ -f "${file_to_chk}" ]; then
			write_info "The file \"${file_to_chk}\" exist" ${WI_DBUG}
		else
			write_info "Can't access to file \"${file_to_chk}\"" ${WI_ERR}
			exit_script "File is missing ?" ${EXIT_ERR_FILE}
		fi
	elif [ "${action_to_do}" = "EXIST_CREATE" ]; then
		if [ -e "${file_to_chk}" ]; then
			write_info "The file \"${file_to_chk}\" exist" ${WI_DBUG}
		else
			write_info "The file \"${file_to_chk}\" doesn't exist" ${WI_DBUG}
			write_info "Creating the file \"${file_to_chk}\"" ${WI_DBUG}
			touch "${file_to_chk}"
			exit_status=${?}
			if [ "${exit_status}" != "0" ]; then
				exit_script "Can't create the file \"${file_to_chk}\"" ${EXIT_ERR_FILE}
			fi
		fi
	elif [ "${action_to_do}" = "DIR_EXIST_CREATE" ]; then
		if [ -d "${file_to_chk}" ]; then
			write_info "The folder \"${file_to_chk}\" exist" ${WI_DBUG}
		else
			write_info "The folder \"${file_to_chk}\" doesn't exist" ${WI_DBUG}
			write_info "Creating the folder \"${file_to_chk}\"" ${WI_DBUG}
			mkdir "${file_to_chk}"
			exit_status=${?}
			if [ "${exit_status}" != "0" ]; then
				exit_script "Can't create the fodler \"${file_to_chk}\"" ${EXIT_ERR_FILE}
			fi
		fi
	elif [ "${action_to_do}" = "CREATE" ]; then
		if [ -e "${file_to_chk}" ]; then
			rm -f "${file_to_chk}" 1>/dev/null 2>&1
			exec_status=${?}
			if [ ${exec_status} -ne 0 ]; then
				exit_script "Can't delete the file \"${file_to_chk}\" (access right ?)" "${EXIT_ERR_FILE}"
			else
				write_info "The file \"${file_to_chk}\" has been removed" ${WI_DBUG}
			fi
		fi
		write_info "The file \"${file_to_chk}\"doesn't exist" ${WI_DBUG}
		write_info "Creating the file \"${file_to_chk}\"" ${WI_DBUG}
		touch "${file_to_chk}"
		exit_status=${?}
		if [ "${exit_status}" != "0" ]; then
			exit_script "Can't create the file \"${file_to_chk}\"  (access right ?)" ${EXIT_ERR_FILE}
		fi
	else
		write_info "Action to do \"${action_to_do}\" unknown" ${WI_ERR}
		exit_script "Check the param sent to the function 'check_file'" ${EXIT_ERR_FCT}
	fi
}

# system_copy	:	Function used to copy a file or a folder
# Args			:	$1 => source object
#					$2 => destination object
function system_copy () {
  local src_object="${1-"${PARAM_NODATA}"}"
  local dst_object="${2-"${PARAM_NODATA}"}"
  local exit_status=""
  write_info "Copy from \"${src_object}\" to \"${dst_object}\"" ${WI_DBUG}
  if [ "${src_object}" != "${PARAM_NODATA}" -a "${dst_object}" != "${PARAM_NODATA}" ]; then
			cp -Rf "${src_object}" "${dst_object}"
			exit_status=${?}
			if [ "${exit_status}" = "0" ]; then
				write_info "[OK] Copy from \"${src_object}\" to \"${dst_object}\" has been done" ${WI_INFO}
			else
				if [ "${STOP_ON_ERROR}" = FALSE ]; then
					write_info "[KO] Error during copy from \"${src_object}\" to \"${dst_object}\"" ${WI_ERR}
				else
					exit_script "[KO] Error during copy from \"${src_object}\" to \"${dst_object}\"" ${EXIT_ERR_FCT}
				fi
			fi
        else
			if [ "${STOP_ON_ERROR}" = FALSE ]; then
				write_info "[KO] Error, at least one argument isn't defined" ${WI_ERR}
				write_info "[KO] Origin : \"${src_object}\" / Destination : \"${dst_object}\"" ${WI_ERR}
			else
				exit_script "[KO] Error during copy from \"${src_object}\" to \"${dst_object}\"" ${EXIT_ERR_FCT}
			fi
	fi
}

# system_chmod	:	Function used to change file or folder access mode
# Args			:	$1 => mode to apply
#					$2 => file or folder concerned
function system_chmod () {
  local mode="${1-"${PARAM_NODATA}"}"
  local target="${2-"${PARAM_NODATA}"}"
  local exit_status=""
  write_info "Changing access mode to \"${target}\" (changing to ${mode})" ${WI_DBUG}
  if [ "${mode}" != "${PARAM_NODATA}" -a "${target}" != "${PARAM_NODATA}" ]; then
    chmod -Rf "${mode}" "${target}"
    exit_status=${?}
    if [ "${exit_status}" = "0" ]; then
      write_info "[OK] Changing access mode to \"${target}\" (changing to ${mode}) has been done" ${WI_INFO}
    else
      if [ "${STOP_ON_ERROR}" = FALSE ]; then
        write_info "[KO] Error during changing access mode to \"${target}\" (changing to ${mode})" ${WI_ERR}
      else
        exit_script "[KO] Error during changing access mode to \"${target}\" (changing to ${mode})" ${EXIT_ERR_FCT}
      fi
    fi
  else
    if [ "${STOP_ON_ERROR}" = FALSE ]; then
      write_info "[KO] Error, at least one argument isn't defined" ${WI_ERR}
      write_info "[KO] Target : \"${target}\" / Asked mode : \"${mode}\"" ${WI_ERR}
    else
      exit_script "[KO] Error during changing access mode to \"${target}\" (changing to ${mode}), at least one argument isn't defined" ${EXIT_ERR_FCT}
    fi
	fi
}

# system_chown	:	Function used to change file or folder owner
# Args			:	$1 => owner to apply (format user:group)
#					$2 => file or folder concerned
function system_chown () {
  local own="${1-"${PARAM_NODATA}"}"
  local target="${2-"${PARAM_NODATA}"}"
  local exit_status=""
  write_info "Changing owner to \"${target}\" (changing to \"${own}\")" ${WI_INFO}
  if [ "${own}" != "${PARAM_NODATA}" -a "${target}" != "${PARAM_NODATA}" ]; then
    chown -Rf "${own}" "${target}"
    exit_status=${?}
    if [ "${exit_status}" = "0" ]; then
        write_info "[OK] Changing owner to \"${target}\" (changing to \"${own}\") has been done" ${WI_INFO}
    else
      if [ "${STOP_ON_ERROR}" = FALSE ]; then
        write_info "[KO] Error when changing owner to \"${target}\" (changing to \"${own}\")" ${WI_ERR}
      else
        exit_script "[KO] Error when changing owner to \"${target}\" (changing to \"${own}\")" ${EXIT_ERR_FCT}
      fi
    fi
  else
    if [ "${STOP_ON_ERROR}" = FALSE ]; then
      write_info "[KO] Error, at least one argument isn't defined" ${WI_ERR}
      write_info "[KO] Target : \"${target}\" / Asked owner : \"${own}\"" ${WI_ERR}
    else
        exit_script "[KO] Error when changing owner to \"${target}\" (changing to \"${own}\"), at least one argument isn't defined" ${EXIT_ERR_FCT}
    fi
	fi
}

# system_delete	:	Function used to delete recursively files or folder aged of more than x days
# Args			:	$1 => target path
#					$2 => target file or directory name (template name)
#					$3 => type (0 - both / 1 - file / 2 - directory)
#					$4 => seniority
#					$5 => ask for confirmation (0 - no / 1 - yes)
function system_delete () {
	local target="${1-"${PARAM_NONE}"}"
	local template_name="${2-"${PARAM_NONE}"}"
	local type="${3-"${PARAM_NONE}"}"
	local seniority="${4-"${PARAM_NONE}"}"
	local confirmation="${5-"${PARAM_NONE}"}"
	#local cmd_erase="rm -f {} \\;"
	local cmd_erase="-delete"
	#local cmd_erase="ls -l {} \\;"
	write_info "Removing object (format '${template_name}') having more than ${seniority} day(s) in '${target}' (type : '${type}' - confirm before deletion : '${confirmation}')" ${WI_DBUG}
	if [ "${confirmation}" = "${PARAM_NONE}" ]; then
		# Missing parameter
		if [ "${STOP_ON_ERROR}" = FALSE ]; then
			write_info "[KO] Error : missing parameter when calling the function 'system_delete', parameters passed : ${target} ${template_name} ${type} ${seniority} ${confirmation}" ${WI_WARN}
		else
			exit_script "[KO] Error : missing parameter when calling the function 'system_delete', parameters passed : ${target} ${template_name} ${type} ${seniority} ${confirmation}" ${EXIT_ERR_ARG}
		fi
	else
		# Delete command generation
		if [ "${type}" = "1" ]; then
			local cmd_find="find ${target} -name ${template_name} -mtime +${seniority} -type f -exec ${cmd_erase}"
		elif [ "${type}" = "2" ]; then
			local cmd_find="find ${target} -name ${template_name} -mtime +${seniority} -type d -exec ${cmd_erase}"
		elif [ "${type}" = "0" ]; then
			local cmd_find="find ${target} -name ${template_name} -mtime +${seniority} -type f -exec ${cmd_erase}"
		else
			# Unknown type
			if [ "${STOP_ON_ERROR}" = FALSE ]; then
				write_info "[KO] Error : unknown type, possible : 0 - both / 1 - file / 2 - folder, given : ${type}" ${WI_WARN}
			else
				exit_script "[KO] Error : unknown type, possible : 0 - both / 1 - file / 2 - folder, given : ${type}" ${EXIT_ERR_ARG}
			fi
		fi
		# Synthesis
		write_info "The file type ${template_name} present in ${target}" ${WI_INFO}
		write_info "and being older than ${seniority} day(s) will be deleted" ${WI_INFO}
		write_info "What will be executed :" ${WI_DBUG}
		write_info "'${cmd_find}'" ${WI_DBUG}
		if [ "${confirmation}" = "1" ]; then
			# Ask for confirmation
			write_info "" ${WI_INFO}
			echo -n "Do you want to proceed (Y/N) ? "
			read ANSWER
			ANSWER=$(echo ${ANSWER} | tr [A-Z] [a-z])
			write_info "" "${WI_INFO}"
			if [ "${ANSWER}" = "n" ]; then
				# User choose not to proceed
				exit_script "[OK] Exiting script (user's choice) : function 'system_delete' - origin : user doesn't want to proceed" ${EXIT_ERR_FCT}
			fi
		fi
		write_info "Deleting ..." ${WI_INFO}
		$(${cmd_find})
		exit_status=${?}
		if [ "${exit_status}" = "0" ]; then
			write_info "[OK] Deleting has been done with success" ${WI_INFO}
		else
			if [ "${STOP_ON_ERROR}" = FALSE ]; then
				write_info "[KO] Error during deleting" ${WI_WARN}
			else
				exit_script "[KO] Error during deleting" ${EXIT_ERR_FCT}
			fi
		fi
	fi
}

# system_delete_lim	:	Function used to delete recursively files or folder aged of more than x days, with max depth = MAXDEPTH (need to be greater than 1)
# Args				:	$1 => target path
#						$2 => target file or directory name (template name)
#						$3 => type (0 - both / 1 - file / 2 - directory)
#						$4 => seniority
#						$5 => ask for confirmation (0 - no / 1 - yes)
function system_delete_lim () {
	local target="${1-"${PARAM_NONE}"}"
	local template_name="${2-"${PARAM_NONE}"}"
	local type="${3-"${PARAM_NONE}"}"
	local seniority="${4-"${PARAM_NONE}"}"
	local confirmation="${5-"${PARAM_NONE}"}"
	local max_depth="${MAXDEPTH-"1"}"
	#local cmd_erase="rm -f {} \\;"
	local cmd_erase="-delete"
	#local cmd_erase="ls -l {} \\;"
	write_info "Removing object (format '${template_name}') having more than ${seniority} day(s) in '${target}' (type : '${type}' - confirm before deletion : '${confirmation}')" ${WI_DBUG}
	if [ "${confirmation}" = "${PARAM_NONE}" ]; then
		# Missing parameter
		if [ "${STOP_ON_ERROR}" = FALSE ]; then
			write_info "[KO] Error : missing parameter when calling the function 'system_delete_lim', parameters passed : ${target} ${template_name} ${type} ${seniority} ${confirmation}" ${WI_WARN}
		else
			exit_script "[KO] Error : missing parameter when calling the function 'system_delete_lim', parameters passed : '${target}' '${template_name}' '${type}' '${seniority}' '${confirmation}'" ${EXIT_ERR_ARG}
		fi
	else
		# Delete command generation
		if [ "${type}" == "1" ]; then
			local cmd_find="/usr/bin/find ${target} -maxdepth ${max_depth} -name ${template_name} -mtime +${seniority} -type f ${cmd_erase}"
		elif [ "${type}" == "2" ]; then
			local cmd_find="find ${target} -maxdepth ${max_depth} -name ${template_name} -mtime +${seniority} -type d -exec ${cmd_erase}"
		elif [ "${type}" == "0" ]; then
			local cmd_find="find ${target} -maxdepth ${max_depth} -name ${template_name} -mtime +${seniority} -type f -exec ${cmd_erase}"
		else
			# Unknown type
			if [ "${STOP_ON_ERROR}" = FALSE ]; then
				write_info "[KO] Error : unknown type, possible : 0 - both / 1 - file / 2 - folder, given : ${type}" ${WI_WARN}
			else
				exit_script "[KO] Error : unknown type, possible : 0 - both / 1 - file / 2 - folder, given : ${type}" ${EXIT_ERR_ARG}
			fi
		fi
		# Synthesis
		write_info "The file type ${template_name} present in ${target} (max depth ${max_depth})" ${WI_INFO}
		write_info "and being older than ${seniority} day(s) will be deleted" ${WI_INFO}
		write_info "What will be executed :" ${WI_DBUG}
		if [ "${confirmation}" == "1" ]; then
			# Ask for confirmation
			write_info "" ${WI_INFO}
			echo -n "Do you want to proceed (Y/N) ? "
			read ANSWER
			ANSWER=$(echo ${ANSWER} | tr [A-Z] [a-z])
			write_info "" "${WI_INFO}"
			if [ "${ANSWER}" = "n" ]; then
				# User choose not to proceed
				exit_script "[OK] Exiting script (user's choice) : function 'system_delete' - origin : user doesn't want to proceed" ${EXIT_ERR_FCT}
			fi
		fi
		write_info "Deleting ..." ${WI_INFO}
		$(${cmd_find})
		exit_status=${?}
		if [ "${exit_status}" = "0" ]; then
			write_info "[OK] Deleting has been done with success" ${WI_INFO}
		else
			if [ "${STOP_ON_ERROR}" = FALSE ]; then
				write_info "[KO] Error during deleting" ${WI_WARN}
			else
				exit_script "[KO] Error during deleting" ${EXIT_ERR_FCT}
			fi
		fi
	fi
}

# system_setfacl_user	:	Function used to add user access to a folder (rw(x) on files + rwx on folders)
# Args					:	$1 => user to add
#							$2 => folder concerned
function system_setfacl_user () {
  local userToAdd="${1-"${PARAM_NODATA}"}"
  local target="${2-"${PARAM_NODATA}"}"
  local exit_status=""
  write_info "Setfacl => Adding user \"${userToAdd}\" to \"${target}\"" ${WI_INFO}
  if [ "${userToAdd}" != "${PARAM_NODATA}" -a "${target}" != "${PARAM_NODATA}" ]; then
		$(find ${target} -type f -exec setfacl -m u:${userToAdd}:rw {} \; 2>&1)
    exit_status=${?}
		if [ "${exit_status}" != "0" ]; then
			if [ "${STOP_ON_ERROR}" = FALSE ]; then
        write_info "[KO] Error when adding \"${userToAdd}\" to \"${target}\" (step 1/3)" ${WI_ERR}
      else
				exit_script "[KO] Error when adding \"${userToAdd}\" to \"${target}\" (step 1/3)" ${EXIT_ERR_FCT}
			fi
		fi
		$(find ${target} -type f -perm -100 -exec setfacl -m u:${userToAdd}:rwx {} \; 2>&1)
    exit_status=${?}
		if [ "${exit_status}" != "0" ]; then
			if [ "${STOP_ON_ERROR}" = FALSE ]; then
        write_info "[KO] Error when adding \"${userToAdd}\" to \"${target}\" (step 2/3)" ${WI_ERR}
      else
				exit_script "[KO] Error when adding \"${userToAdd}\" to \"${target}\" (step 2/3)" ${EXIT_ERR_FCT}
			fi
		fi
		$(find ${target} -type d -exec setfacl -m u:${userToAdd}:rwx {} \; 2>&1)
    exit_status=${?}
		if [ "${exit_status}" != "0" ]; then
			if [ "${STOP_ON_ERROR}" = FALSE ]; then
        write_info "[KO] Error when adding \"${userToAdd}\" to \"${target}\" (step 3/3)" ${WI_ERR}
      else
				exit_script "[KO] Error when adding \"${userToAdd}\" to \"${target}\" (step 3/3)" ${EXIT_ERR_FCT}
			fi
		fi
    write_info "[OK] Adding user \"${userToAdd}\" to \"${target}\"" ${WI_INFO}
  else
    if [ "${STOP_ON_ERROR}" = FALSE ]; then
      write_info "[KO] Error, at least one argument isn't defined" ${WI_ERR}
      write_info "[KO] Target : \"${target}\" / Asked user to add (setfacl) : \"${userToAdd}\"" ${WI_ERR}
    else
        exit_script "[KO] Error when adding user \"${userToAdd}\" to \"${target}\"" ${EXIT_ERR_FCT}
    fi
	fi
}

# ------------ Write on screen functions -----------

# write_msg	:	Function used to write on screen (formated message - no log)
# Args		:	$1 => message to print
#				$2 => type of message (Debug=5; Info=4; Warn=3; Error=2; None=0)
function write_msg () {
	local msg=${1-"No message to show"}
	local msg_init=${1}
	local level=${2-4}
	local screen_display=${SCREEN_DISPLAY-FALSE}
	local debug_level=${DEBUG_LEVEL-4}
	if [ ${level} -le ${debug_level} ]; then
		case ${level} in
			"0")
				msg="> ${msg}"
	    		;;
	  		"2")
				msg="ERR  : ${msg}"
	    		;;
	  		"3")
				msg="WARN : ${msg}"
	    		;;
	  		"4")
				msg="INFO : ${msg}"
	    		;;
	  		"5")
				msg="DBUG : ${msg}"
	    		;;
	  		*)
				msg="> ????? > ${msg}"
				;;
		esac
		if [ "${screen_display}" = TRUE ]; then
			if [ "${msg_init}" = "" ]; then
				echo "${msg_init}"
			else
				echo "${msg}"
			fi
		fi
	fi
}

##################### OBSOLETE FUNCTION #####################
# write_scr	:	Function used to write on screen + debug msg (formated message - no log), use DEBUG_MODE - kept back for compatibility reason
# Args		:	$1 => message to write
#				$2 => debug message (optional)
# Comments :	if DEBUG_MODE is define, the function add a debug message saying the function is called
#				Need SHOW_MSG_ON_SCREEN equals to TRUE in order to show something on screen
function write_scr () {
	local message="Calling obsolete function 'write_scr' use 'write_msg' instead - msg : ${1}"
	local message_debug=${2:-"Debug - Appel de 'write_scr' : Affichage à l'écran"}
	# Check if global vars are defined, if not, use default (debug_mode off / cron_mode off)
	local debug_mode=${DEBUG_MODE:-0}
	if [ "$debug_mode" -gt 0 ]; then
		write_msg "$message_debug" ${WI_WARN}
	fi
	# Call write_msg
	write_msg "${message}" ${WI_WARN}
}
##################### End of OBSOLETE FUNCTION ##############

# write_info	:	Function used to write on screen (formated message with differents formats)
#					and to write on log
# Args			:	$1 => message to print
#					$2 => type of message (Debug=5; Info=4; Warn=3; Error=2; None=0)
function write_info () {
	local msg=${1-"No message to print"}
	local msg_init=${1}
	local level=${2-"0"}
	local screen_display=${SCREEN_DISPLAY-FALSE}
	local debug_level=${DEBUG_LEVEL-4}
	if [ ${level} -le ${debug_level} ]; then
		case ${level} in
			0)
				msg="> ${msg}"
	    	;;
	  	2)
				msg="ERR  : ${msg}"
	    	;;
	  	3)
				msg="WARN : ${msg}"
	    	;;
	  	4)
				msg="INFO : ${msg}"
	    	;;
	  	5)
				msg="DBUG : ${msg}"
	    	;;
	  	*)
				msg="> ????? > ${msg}"
				;;
		esac
		if [ "${screen_display}" = TRUE ]; then
			if [ "${msg_init}" = "" ]; then
				echo "${msg_init}"
			else
				echo "${msg}"
				write_log "${msg}"
			fi
		else
			if [ "${msg_init}" != "" ]; then
				write_log "${msg}"
			fi
		fi
	fi
}

##################### OBSOLETE FUNCTION #####################
# write_msg_2	:	Function used to write on screen (formated message with differents formats) - kept back for compatibility reason
# Args			:	$1 => message to print
#					$2 => type of message (Debug=5; Info=4; Warn=3; Error=2; None=0)
function write_msg_2 () {
  local msg=${1-"No message to print"}
	msg="Calling obsolete function 'write_msg' - msg : ${msg}"
	local display_type=${2-"${WI_MINI}"}
	# Call write_info
	write_info ${msg} ${display_type}
}
##################### End of OBSOLETE FUNCTION ##############

# welcome_msg	:	Function used to show welcome' script message
# Args			:	$1 => description
#					$2 => script name
#					$3 => version
#					$4 => author
#					$5 => last modification date
#					$6 => license
#					$7 => running env
function welcome_msg () {
	local desc=${1-"Description"}
	local name=${2-"Script Name"}
	local vers=${3-"Version"}
	local author=${4-"Author"}
	local modif_date=${5-"Last modification date"}
	local license=${6-"License"}
	local env=${7-"Env"}
	local screen_display=${SCREEN_DISPLAY-FALSE}
	if [ "${screen_display}" = TRUE ]; then
		echo ""
		echo "-------------------------------------------------------------------------------"
		echo ""
		echo " ${desc}"
		echo ""
		echo "-------------------------------------------------------------------------------"
		echo ""
		echo "   Script Name       : ${name}"
		echo "   Version           : ${vers}"
		echo "   Author            : ${author}"
		echo "   Last update       : ${modif_date}"
		echo "   Licence           : ${license}"
		echo "   Env               : ${env}"
		echo ""
		echo "-------------------------------------------------------------------------------"
		echo ""
	fi
	write_log "Script '${name}' vers. ${vers} launched"
}

# ------------ Write on log functions ---------------

# write_log	:	Function used to write on log file, use LOG_MAKE, LOG_FILE,
#  				LOG_FOR_MAIL, LOG_MAIL_FILE, LOG_FIRST_RUN and LOG_MODE
# Args		:	$1 => message to write on log
function write_log () {
	local msg=${1-"No message"}
	local date_time=$(date +%Y-%m-%d-%R)
	local log_make=${LOG_MAKE-FALSE}
	local log_file=${LOG_FILE-"/tmp/no_file.log"}
	local log_first_run=${LOG_FIRST_RUN}
	local log_mode=${LOG_MODE-"666"}
	local exec_status=""
	# If logs are activated, write on file
	if [ "${log_make}" = TRUE ]; then
		# Check if the log file exist
		if [ -e "${log_file}" ]; then
			# Check if the log file is writable - if not, exit
			if [ ! -w "${log_file}" ]; then
				exit_script "Can't write in the file '${log_file}'" ${EXIT_ERR_FILE}
			fi
		else # Create the file
			touch "${log_file}" >/dev/null 2>&1
			exec_status=${?}
			# file creation NOK => exit
			if [ ${exec_status} -ne 0 ]; then
				exit_script "Can't create the log file '${log_file}' (access right error ?)" ${EXIT_ERR_FILE}
			else
				write_msg "Log file '${log_file}' created" ${WI_INFO}
			fi
			# change access mode
			chmod ${log_mode} ${log_file}
			exec_status=${?}
			if [ ${exec_status} -ne 0 ]; then
				exit_script "Can't change access right on log file '${log_file}'" ${EXIT_ERR_FILE}
			else
				write_msg "Access right on the log file '${log_file}' changed (to '${log_mode}')" ${WI_INFO}
			fi
		fi
		# Check if it's the first call to the function in the script
		if [ "${log_first_run}" -eq 1 ]; then
			echo "-------------------------------------------------------------------------------" >> ${log_file}
			log_first_run=0
		fi
		echo "$date_time : ${msg}" >> ${log_file}
		LOG_FIRST_RUN=${log_first_run}
	fi
}

# ------------ Apps functions -----------------------

# check_mysql_env	:	Function used to check if the MySQL environment is correct
# Args				:	$1 => MySQL environment to check (fde/dev/int/inv/sta/sin/pro)
function check_mysql_env () {
	local mysql_env=${1,,}
	# the ',,' is used in order to lowercase the arg, to uppercase, use '^^'
	# OK for bash > 4.0, for bash < 4.0, use something like
	# myvar=$(echo "${myvar}"|awk '{print toupper($0)}') or myvar=$(echo "${myvar}"| tr '[:lower:]' '[:upper:]')
	FCT_EXEC_STATUS=1
	case "${mysql_env}" in
		fde) CHECK_MYSQL_ENV=TRUE
			write_info "MySQL env : ${mysql_env}" ${WI_DBUG}
			;;
		dev) CHECK_MYSQL_ENV=TRUE
			write_info "MySQL env : ${mysql_env}" ${WI_DBUG}
			;;
		int) CHECK_MYSQL_ENV=TRUE
			write_info "MySQL env : ${mysql_env}" ${WI_DBUG}
			;;
		inv) CHECK_MYSQL_ENV=TRUE
			write_info "MySQL env : ${mysql_env}" ${WI_DBUG}
			;;
		sta) CHECK_MYSQL_ENV=TRUE
			write_info "MySQL env : ${mysql_env}" ${WI_DBUG}
			;;
		sin) CHECK_MYSQL_ENV=TRUE
			write_info "MySQL env : ${mysql_env}" ${WI_DBUG}
			;;
		pro) CHECK_MYSQL_ENV=TRUE
			write_info "MySQL env : ${mysql_env}" ${WI_DBUG}
			;;
		*) exit_script "Error, allowed MySQL env are : fde, dev, int, inv, sta, sin or pro" ${EXIT_ERR_MYSQL}
			;;
	esac
	FCT_EXEC_STATUS=0
}

# get_build_status	:	Function used to get the build status of a Jenkins job, use BUILD_STATUS
# Args				:	$1 => file where to search the $2 string
#						$2 => string to search in the $1 file
# Note				:	$1 existence has to be checked before
function get_build_status () {
	local file=${1}
	local str_search=${2}
	local exec_status=""
	FCT_EXEC_STATUS=1
	if [ "${file}" = "" -o "${str_search}" = "" ]; then
		BUILD_STATUS="FAILED"
		write_info "[KO] Either the file (given : '${file}') or the string to search (given : '${str_search}') have not been provided" ${WI_DBUG}
	else
		cat "${file}" | grep "${str_search}" >/dev/null 2>&1
		exec_status=${?}
		if [ "${exec_status}" == "0" ]; then
		BUILD_STATUS="SUCCESS"
			write_info "[OK] The build is successful, the string to search ('${str_search}') has been found in the file '${file}'" ${WI_DBUG}
		else
			BUILD_STATUS="FAILED"
				write_info "[FAILED] The build is NOT successful, the string to search ('${str_search}') has NOT been found in the file '${file}'" ${WI_DBUG}
		fi
	fi
	FCT_EXEC_STATUS=0
}

# ------------------------------------------------------
# ------------ End of functions ----------------------
# ------------------------------------------------------
