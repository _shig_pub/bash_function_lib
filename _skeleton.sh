#!/bin/bash

###############################################################
#
# Skeleton script
#
# Script_name   : _skeleton.sh
# Arg           : None
#
# /!\ Suitable with function.sh > v0.25 only /!\
#
###############################################################

# ------------ Global Var --------------------------

# Script name
NAME=`basename ${0}`
# Script role
DESCR="Skeleton script / example"
# Version
VERS="0.5"
# Author
AUTHOR="JVA"
# Last modification
DATEMODIF="2022/07/11"
# Licence
LICENCE="GPL v3"
# Env
ENV="GNU/Linux"

# Log var
LOG_MAKE="TRUE"
#LOG_FILE="/var/log/${NAME}.log"
LOG_FILE="/tmp/${NAME}.log"

# Some vars
DEBUG_LEVEL=5           # Debug=5; Info=4; Warn=3; Error=2; None=0
FCT_EXEC_STATUS=0       # Function execution status : Used to know the result after a function call if she doesn't show message (mandatory)
SCRIPT_NAME=""          # If function get_script_name is used (not mandatory)
BUILD_STATUS="FAILED"   # For Jenkins scripts (not mandatory, in function get_build_status)
DEBUG_MODE=0            # If write_scr function is used (not mandatory)

# Sourcing functions
FCT_FOLDER="/opt/scripts"
FCT_FILE="${FCT_FOLDER}/functions.sh"
if [ -f ${FCT_FILE} ]; then
        # Source the file
        . ${FCT_FILE}
else
        # Exit because file is missing
        echo ""
        echo "/!\\ Functions file missing /!\\"
        echo "/!\\ File needed : \"${FCT_FILE}\" /!\\"
        echo "/!\\ Exiting script with error (5) /!\\"
        echo ""
        exit 5
fi

# Display message on screen
SCREEN_DISPLAY=TRUE
# Verbose screen mode
SCR_VERBOSE="TRUE"
# Stop if error ?
STOP_ON_ERROR=FALSE

# Send mail via OpenSSL params
MAIL_USER=""  # need to use '\' before special char, example : MyPwd\$1 for MyPwd$1
MAIL_PWD=""   # need to use '\' before special char, example : MyPwd\$1 for MyPwd$1
MAIL_PORT_SECURE=465
RELAY="my.relay.srv"
FROM="${FROM-"some@one.com"}"
TO="${TO-"other@one.com"}"
TITLE="${TITLE-"subject: Test mail from $(hostname)"}"
BODY="${BODY-"Test mail from $(hostname)"}"

# Get script name
get_script_name "${0}"

# ------------ End of var -------------------------------

# ------------ Functions : begining ---------------------

# ------------ Functions : end --------------------------

# ------------ Main Prog : Begining ---------------------

# Welcome screen
welcome_msg "${DESCR}" "${NAME}" "${VERS}" "${AUTHOR}" "${DATEMODIF}" "${LICENCE}" "${ENV}"

# If needed, check if running script as root
check_root

# Send mail example
BODY="${BODY} with the script '${SCRIPT_NAME}'"
send_openssl_mail "${RELAY}" "${FROM}" "${TO}" "${TITLE}" "${BODY}"

# Write log info example
write_info "" ${WI_INFO}
write_info "End of script" ${WI_INFO}
write_info "" ${WI_INFO}

exit ${EXIT_OK}

# ------------ Main Prog : End --------------------------
