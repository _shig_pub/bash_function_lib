# bash_function_lib

Some functions for bash scripts


## Name
bash functions library

## Description
Store in a bash lib file some bash functions

## Installation
Copy the functions.sh file and _skeleton.sh file into your working directory.
Rename _skeleton.sh file as you want.
Change var if needed
Use it

## Roadmap
According to my ideas and needs

## License
GNU/GPLv3

## Project status
Running, when I have time :-)
